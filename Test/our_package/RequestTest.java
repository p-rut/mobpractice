package our_package;

import static org.junit.Assert.assertEquals;

import java.util.Random;
import org.junit.Test; 

public class RequestTest {

	@Test
	public void test() {

		String URL = "https://jsonplaceholder.typicode.com/albums?userId=";
		Random randomUserId = new Random();
		int userId = randomUserId.nextInt(100);
		String expected = URL + userId;
		URL url = new URL(userId);
		
		String actual = url.getUrl();
	
		assertEquals(expected, actual);
		
	}
	
	@Test
	public void testGetRequest() {
		
		
		
	}

}
