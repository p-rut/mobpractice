package our_package;

import java.util.Random;

public class URL {
	
	private int userId;
	
	public URL(int _userId) {
		userId = _userId;
	}

	public String getUrl() {
		
		return "https://jsonplaceholder.typicode.com/albums?userId=" + userId;
		
	}


}
